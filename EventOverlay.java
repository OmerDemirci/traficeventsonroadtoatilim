package trafic.events.onroadto.atilim;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;

public class EventOverlay extends ItemizedOverlay<OverlayItem>{
	int selectedLatitude;
	 int selectedLongitude;
	 private GeoPoint point;
	private ArrayList<OverlayItem> overlays = 
			new ArrayList<OverlayItem>();
	private Context context;
	public EventOverlay(Drawable marker) {
		this(null,marker,null);
		
	}
	
	public EventOverlay(Context context, Drawable marker,GeoPoint point) {
		super(boundCenterBottom(marker));
		this.context = context;
		this.point=point;
	}
	protected OverlayItem createItem(int index) {
		return overlays.get(index);
	}
	public void addOverlay(OverlayItem overlay){
		overlays.add(overlay);
		populate();
	}
	@Override
	public int size() {
		return overlays.size();
	}
	
	public boolean onTap(int index,GeoPoint geoPoint, MapView mapView) {
		selectedLatitude = geoPoint.getLatitudeE6(); 
       selectedLongitude = geoPoint.getLongitudeE6();
        
		OverlayItem item = overlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.setPositiveButton("Back to Map", new OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();				
			}
		});
		dialog.show();
		
		return super.onTap(geoPoint,mapView);
		//return true;
	}
	
	public void draw(Canvas canvas, MapView mapV, boolean shadow){

		Projection proj = mapV.getProjection();
        GeoPoint loc = point;

        Point point = new Point();
        proj.toPixels(loc, point);
        float rad = (float) (proj.metersToEquatorPixels(150) * (1 / Math.cos(Math.toRadians(loc.getLatitudeE6() / 1000000))));

        Paint circle = new Paint();
        circle.setColor(0x30000000);
        circle.setAlpha(30);
        circle.setAntiAlias(true);
        circle.setStyle(Style.FILL);

        canvas.drawCircle(point.x, point.y, rad, circle);

        super.draw(canvas, mapV, shadow);
        
	}
}
