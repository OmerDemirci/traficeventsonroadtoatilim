package trafic.events.onroadto.atilim;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public class EventDetails extends SoapObject implements KvmSerializable {
	int longtitude;
	int latitude;
	String type;
	String submitter;
	EventDetails(int longtitude,int latitude,String type,String submitter){
		super(null, null);
		this.longtitude=longtitude;
		this.latitude=latitude;
		this.type=type;
		this.submitter=submitter;
	}
	public EventDetails() {
		super(null, null);
		// TODO Auto-generated constructor stub
	}
	public int getLongtitude() {
		return longtitude;
	}
	public void setLongtitude(int longtitude) {
		this.longtitude = longtitude;
	}
	public int getLatitude() {
		return latitude;
	}
	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubmitter() {
		return submitter;
	}
	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}
public Object getProperty(int arg0) {
        
        switch(arg0)
        {
        case 0:
            return latitude;
        case 1:
            return longtitude;
        case 2:
            return submitter;
        case 3:
            return type;
        }
        
        return null;
    }
public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
    switch(index)
    {
    case 0:
        info.type = PropertyInfo.INTEGER_CLASS;
        info.name = "latitude";
        break;
    case 1:
        info.type = PropertyInfo.INTEGER_CLASS;
        info.name = "longtitude";
        break;
    case 2:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "submitter";
        break;
    case 3:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "type";
        break;
    default:break;
    }
}
public void setProperty(int index, Object value) {
	
    switch(index)
    {
    case 0:
    	latitude = Integer.parseInt(value.toString());
        break;
    case 1:
    	longtitude = Integer.parseInt(value.toString());
        break;
    case 2:
    	submitter = value.toString();
        break;
    case 3:
    	type = value.toString();
        break;
    default:
        break;
    }
}
}//latitude
//longtitude
//type
//submitter
