package trafic.events.onroadto.atilim;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;


public class UserLocationOverlay extends ItemizedOverlay<OverlayItem>{
private Location location;
private Context context;
private RadioButton carAccident;
private RadioButton iceness;
private RadioButton roadConstruction;
private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
private final String NAMESPACE = "http://webservice.atilim.tort";
private final String URL = "http://192.168.195.1:8080/TraficsOnRoadToAtilimWebService/services/Functions?wsdl";
private final String METHOD_NAME = "insertToEvent";
private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
private int lat,longt;
private String userName;
public UserLocationOverlay(Drawable defaultMarker,Context context,int lat,int longt,String userName) {
	super(boundCenterBottom(defaultMarker));	
	this.context = context;	
	this.lat=lat;	
	this.longt=longt;
	this.userName=userName;
}

// Executed, when populate() method is called
@Override
protected OverlayItem createItem(int arg0) {
	return mOverlays.get(arg0);		
}

@Override
public int size() {		
	return mOverlays.size();
}

public void addOverlay(OverlayItem overlay){
	mOverlays.add(overlay);
	populate(); // Calls the method createItem()
}

@Override
protected boolean onTap(int index) {
	OverlayItem item = mOverlays.get(index);
	AlertDialog.Builder dialog = new AlertDialog.Builder(context);
	dialog.setTitle(item.getTitle());
	dialog.setMessage(item.getSnippet());
	LayoutInflater factory = LayoutInflater.from(context);
	View view = factory.inflate(R.layout.insertevent, null);
	carAccident=(RadioButton)view.findViewById(R.id.Accident);
	iceness=(RadioButton)view.findViewById(R.id.Iceness);
	roadConstruction=(RadioButton)view.findViewById(R.id.RoadConstruction);
	dialog.setView(view);
	
	dialog.setNeutralButton("Insert Event", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {
            	if(carAccident.isChecked()){
            		InsertEventAction("Car Accident",longt,lat,userName);
				}else if(iceness.isChecked()){
					InsertEventAction("Insert Event Action",longt,lat,userName);
				}else if(roadConstruction.isChecked()){
					InsertEventAction("Road Construction",longt,lat,userName);
				}
            }
        });

	dialog.show();
	return true;
}
private String InsertEventAction(String eventType, int longtitude, int latitude,String submitter){
	Object result="";
     try
 	{
    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
     request.addProperty("eventType", eventType);
 	request.addProperty("submitter",  submitter);
 	request.addProperty("longtitude",  longtitude);
 	request.addProperty("latitude",  latitude);
 	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
 	envelope.dotNet = true;
 	envelope.setOutputSoapObject(request);
 	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
 	androidHttpTransport.call(SOAP_ACTION,envelope);
 	 result = envelope.getResponse();
 	
     
 	}
     catch (Exception E) {
    		E.printStackTrace();
    		Log.d("hata",E.getClass().getName() + ":" + E.getMessage());
    		}
    return result.toString();
 }
}
