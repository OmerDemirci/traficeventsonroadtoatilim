package trafic.events.onroadto.atilim;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
public class GoogleMap extends MapActivity implements LocationListener{
	private String locationService;
	private HttpTransport k;
	private MapView mapView;
	private LocationManager locationManager;
	private String locationProvider;
	private Location location;
	private MapController mapController;
	private UserLocationOverlay userLocationOverLay;
	private List<Overlay> overlays;
	private String userName;
	private final String NAMESPACE = "http://webservice.atilim.tort";//
	private final String URL = "http://192.168.195.1:8080/TraficsOnRoadToAtilimWebService/services/Functions?wsdl";
	private final String METHOD_NAME = "getEventList";
	private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
	private ArrayList<EventDetails> details;
	private EventOverlay itemizedOverlay;
	private MapController controller;
	 private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in Meters
     private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in Milliseconds
     private static final long POINT_RADIUS = 1000; // in Meters
     private static final long PROX_ALERT_EXPIRATION = -1; // It will never expire
     private static final String PROX_ALERT_INTENT = "com.androidmyway.demo.ProximityAlert";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		mapView = (MapView)findViewById(R.id.mapview);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		userName = getIntent().getStringExtra("UserName");		
		controller = mapView.getController();
		getEventData();
		mapView.setBuiltInZoomControls(true);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);
        if(location!=null){
        	onLocationChanged(location);
        }
       
        locationManager.requestLocationUpdates(provider, 20000, 0, this);
        
    }

  
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {

		double latitude = location.getLatitude();

		double longitude = location.getLongitude();

		GeoPoint point = new GeoPoint((int)(latitude * 1E6), (int)(longitude*1E6));

		MapController mapController = mapView.getController();
		mapController.animateTo(point);
		mapController.setZoom(15);
		
		mapView.invalidate();
		
		List<Overlay> mapOverlays = mapView.getOverlays();
	
		Drawable drawable = this.getResources().getDrawable(R.drawable.cur_position);
		
		UserLocationOverlay currentLocationOverlay = new UserLocationOverlay(drawable,this,(int)(latitude * 1E6),(int)(longitude*1E6),userName);
		
		OverlayItem currentLocation = new OverlayItem(point, "Traffic Events","Chosee list below in order to insert event");

		currentLocationOverlay.addOverlay(currentLocation);
		
		mapOverlays.clear();
		
		mapOverlays.add(currentLocationOverlay);	
		 List<Overlay> overlays = mapView.getOverlays();
	        details=getEventData();
	        for(int i=0;i<details.size();i++){
	        	GeoPoint[] points = new GeoPoint[details.size()];
	        	points[i]= new GeoPoint(details.get(i).getLatitude(),details.get(i).getLongtitude());
	        	addProximityAlert((double)details.get(i).getLatitude()/1E6,(double)details.get(i).getLongtitude()/1E6);
	        	itemizedOverlay = new EventOverlay(this,
	            		getResources().getDrawable(R.drawable.unlem),points[i]);
	        	
	        	OverlayItem[] oItem = new OverlayItem[details.size()];
	        	oItem[i]=new OverlayItem(points[i],details.get(i).getType(),details.get(i).getSubmitter());
	        	itemizedOverlay.addOverlay(oItem[i]);
	        	mapView.postInvalidate();
	        }
	        overlays.add(itemizedOverlay);
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub		
	}
	private ArrayList<EventDetails> getEventData(){
		EventDetails eventData=null;
	        SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
	        ArrayList<EventDetails>eventList=new ArrayList<EventDetails>();
	     
	        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	        envelope.dotNet = true;
	        
	        envelope.setOutputSoapObject(Request);

	        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	        androidHttpTransport.debug = true;
	        try
	        {	
	        	
	        	 androidHttpTransport.call(SOAP_ACTION, envelope);

	          
	             SoapObject response = (SoapObject) envelope.bodyIn;

	             final int intPropertyCount = response.getPropertyCount();
	 	      
	 	      for(int i=0; i<intPropertyCount;i++){
	 	    	  eventData = new EventDetails();
	 	    	 SoapObject responseChild = (SoapObject) response.getProperty(i);
	 	    	 eventData.setProperty(0, responseChild.getProperty(0));
	 	    	 eventData.setProperty(1, responseChild.getProperty(1));
	 	    	 eventData.setProperty(2, responseChild.getProperty(2));
	 	    	 eventData.setProperty(3, responseChild.getProperty(3));
	 	    	eventList.add(eventData);
	 	      }  
 
	        }

	        catch(Exception e)
	        {
	            e.printStackTrace();
	            Log.d("ERROR:","ERROR:"    + e.getClass().getName() + ":" + e.getMessage());
	            
	        }
	        return eventList;
	    }
	 private void addProximityAlert(double latitude,double longitude) {
        
         
         Intent intent = new Intent(PROX_ALERT_INTENT);
         PendingIntent proximityIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
locationManager.addProximityAlert(
                latitude, // the latitude of the central point of the alert region
                longitude, // the longitude of the central point of the alert region
                POINT_RADIUS, // the radius of the central point of the alert region, in meters
                PROX_ALERT_EXPIRATION, // time for this proximity alert, in milliseconds, or -1 to indicate no                           expiration
                proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
         );

         IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
         registerReceiver(new ProximityIntentReceiver(GoogleMap.this), filter);
         Toast.makeText(getApplicationContext(),"Alert Added",Toast.LENGTH_SHORT).show();
  }
	
}
