package trafic.events.onroadto.atilim;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewUser extends Activity{
	private EditText name;
	private EditText surname;
	private EditText username;
	private EditText password;
	private EditText email;
	private EditText secretq;
	private EditText secretanswer;
	private Button create;
	private Button backToMenu;
	private TextView statusE;
	private final String NAMESPACE = "http://webservice.atilim.tort";//
	//local 192.168.1.2
	//connectify 192.168.12.1
	private final String URL = "http://192.168.195.1:8080/TraficsOnRoadToAtilimWebService/services/Functions?wsdl";
	private final String METHOD_NAME = "insertToUser";
	private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newuser);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		name=(EditText) findViewById(R.id.name);
		surname=(EditText) findViewById(R.id.surname);
		username=(EditText) findViewById(R.id.username);
		password=(EditText) findViewById(R.id.password);
		email=(EditText) findViewById(R.id.email);
		secretq=(EditText) findViewById(R.id.secretq);
		secretanswer=(EditText) findViewById(R.id.secretanswer);
		create=(Button) findViewById(R.id.create);
		backToMenu=(Button) findViewById(R.id.backToMenu);
		statusE=(TextView) findViewById(R.id.status);
		create.setOnClickListener(new View.OnClickListener() {
		    
			   public void onClick(View arg0) {
				   insertUserAction();
			      }
			  });
		backToMenu.setOnClickListener(new View.OnClickListener() {
		    
			   public void onClick(View arg0) {
				   Intent intent = new Intent(NewUser.this ,MainActivity.class);
	                startActivity(intent);
			      }
			  });
	}
	public void insertUserAction(){
		Object result="";
	     String user_Name = name.getText().toString();
	     String user_Surname = surname.getText().toString();
	     String user_User = username.getText().toString();
	     String user_Password = password.getText().toString();
	     String user_Email = email.getText().toString();
	     String user_SecretQ = secretq.getText().toString();
	     String user_SecretA = secretanswer.getText().toString();
	     try
	 	{
	    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	     request.addProperty("name", user_Name);
	 	request.addProperty("surname",  user_Surname);
	 	request.addProperty("username",  user_User);
	 	request.addProperty("password",  user_Password);
	 	request.addProperty("email",  user_Email);
	 	request.addProperty("squestion",  user_SecretQ);
	 	request.addProperty("sanswer",  user_SecretA);
	 	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	 	envelope.dotNet = true;
	 	envelope.setOutputSoapObject(request);
	 	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	 	androidHttpTransport.call(SOAP_ACTION,envelope);
	 	 result = envelope.getResponse();
	 	statusE.setText("Status :"+result.toString());
	     
	 	}
	     catch (Exception E) {
	    		E.printStackTrace();
	    		statusE.setText("ERROR:"    + E.getClass().getName() + ":" + E.getMessage());
	    		}
	   
	 }
	
}
