package trafic.events.onroadto.atilim;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	private EditText userName;
	private EditText password;
	private TextView statusE;
	private Button login;
	private Button newUser;
	//"http://192.168.195.1:8080/TraficsOnRoadToAtilimWebService/services/Functions?wsdl";
	private final String NAMESPACE = "http://webservice.atilim.tort";//
	private final String URL = "http://192.168.195.1:8080/TraficsOnRoadToAtilimWebService/services/Functions?wsdl";
	private final String METHOD_NAME = "authentication";
	private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		userName=(EditText) findViewById(R.id.username);
		password=(EditText) findViewById(R.id.password);
		
		statusE=(TextView) findViewById(R.id.status);
		login=(Button) findViewById(R.id.login);
		newUser=(Button) findViewById(R.id.newuser);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		 login.setOnClickListener(new View.OnClickListener() {
			    
			   public void onClick(View arg0) {
				   String status=loginAction();	
				   if(status.equals("Success!")){
					   Intent intent = new Intent(MainActivity.this ,GoogleMap.class);
					   intent.putExtra("UserName",userName.getText().toString());
		                startActivity(intent);
				   }
			      }
			  });
		 newUser.setOnClickListener(new View.OnClickListener() {
			    
			   public void onClick(View arg0) {
				   Intent intent = new Intent(MainActivity.this,NewUser.class);
					startActivity(intent);	
					   
				   
			      }
			  });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	private String loginAction(){
		Object result="";
	     String user_Name = userName.getText().toString();
	     String user_Password = password.getText().toString();
	     try
	 	{
	    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	     request.addProperty("userName", user_Name);
	 	request.addProperty("password",  user_Password);
	 	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	 	envelope.dotNet = true;
	 	envelope.setOutputSoapObject(request);
	 	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	 	androidHttpTransport.call(SOAP_ACTION,envelope);
	 	 result = envelope.getResponse();
	 	statusE.setText("Status :"+result.toString());
	     
	 	}
	     catch (Exception E) {
	    		E.printStackTrace();
	    		statusE.setText("ERROR:"    + E.getClass().getName() + ":" + E.getMessage());
	    		}
	    return result.toString();
	 }
}
